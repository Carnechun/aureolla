import './App.css';
import Home from './pages/Home';
import Routes from './pages/Routes';
import StudentContext,{api_path} from './api/Student';
function App() {
  return (


    <StudentContext.Provider value={api_path}>
        <div className="App">
          <Routes/>
        </div>
    </StudentContext.Provider>

    
  );
}

export default App;
