import React, {Component,useState,useEffect} from "react";
import { FormControl,InputLabel,Input,FormHelperText } from '@mui/material';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';



export default function CardStudentCreate(props)
{

    //hook
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [serial_number, setSerial_number] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [gender, setGender] = useState('');

    useEffect(() => {
        
        return () => {
            console.log(surname);
        };
    }, [surname]);

    const createStudentClick = ()=>
    {
        alert('alumno creado '+name);
    }



    return(
        <>
        <Card sx={{ minWidth: 275, marginTop: 5 ,marginLeft:5}}>
      <CardContent sx={{ padding: 3}}>

      <FormControl fullWidth sx={{marginBottom:3}}>
            <InputLabel htmlFor="name-input">Name</InputLabel>
            <Input onChange={(e) => setName(e.target.value)} id="name-input" />
        </FormControl>

        <FormControl fullWidth sx={{marginBottom:3}}>
            <InputLabel htmlFor="surname-input">Surname</InputLabel>
            <Input onChange={(e) => setSurname(e.target.value)} id="surname-input" />
        </FormControl>

        <FormControl fullWidth sx={{marginBottom:3}}>
            <InputLabel htmlFor="serial-number-input">Serial number</InputLabel>
            <Input onChange={(e) => setSerial_number(e.target.value)} id="serial-number-input" />
        </FormControl>

        <FormControl fullWidth sx={{marginBottom:3}}>
            <InputLabel htmlFor="email-input">Email address</InputLabel>
            <Input onChange={(e) => setEmail(e.target.value)} id="email-input" aria-describedby="helper-email" />
            <FormHelperText id="helper-email">We'll never share your email.</FormHelperText>
        </FormControl>

        <FormControl fullWidth sx={{marginBottom:3}}>
            <InputLabel htmlFor="phone-number-input">Phone number</InputLabel>
            <Input onChange={(e) => setPhone(e.target.value)} id="phone-number-input" />
        </FormControl>

        <FormControl fullWidth sx={{marginBottom:3}}>
            <InputLabel htmlFor="gender-input">Gender</InputLabel>
            <Input onChange={(e) => setGender(e.target.value)} id="gender-input" />
        </FormControl>

      </CardContent>
      <CardActions sx={{paddingLeft:3}}>
            <Button onClick={createStudentClick} variant="contained">Create</Button>
      </CardActions>
    </Card>
        </>
    );
}