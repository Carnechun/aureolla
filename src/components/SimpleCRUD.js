import React,{useState,useEffect,Component,useContext} from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import SaveIcon from '@mui/icons-material/Save';
import SendIcon from '@mui/icons-material/Send';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import constants from '../static/constants';
import StudentContext from "../api/Student";

export default function SimpleCRUD(props)
{
    

    return(
        <StudentCRUD></StudentCRUD>
    );
    
}

function StudentCRUD(props)
{
    const Student = useContext(StudentContext);

    useEffect(() => {
        
         
            Student[constants.INDEX]();
    
    }, []);
    const deleteClick = (serial_number) =>
    {
        alert(serial_number);
    }
    function createData(name, surname, serial_number, email, phone, gender) {
        return { name, surname, serial_number, email, phone, gender };
      }
      
      const rows = [
        createData('Mirna Raquel', 'Valdivia', '0316108665', 'mirna.validvia@miutt.edu.mx', '6642598479','F'),
        createData('Mirna Raquel', 'Valdivia', '0316108665', 'mirna.validvia@miutt.edu.mx', '6642598479','F'),
        createData('Mirna Raquel', 'Valdivia', '0316108665', 'mirna.validvia@miutt.edu.mx', '6642598479','F'),
        createData('Mirna Raquel', 'Valdivia', '0316108665', 'mirna.validvia@miutt.edu.mx', '6642598479','F'),
        createData('Mirna Raquel', 'Valdivia', '0316108665', 'mirna.validvia@miutt.edu.mx', '6642598479','F'),
      ];
      return (
        <Paper sx={{ width: '100%', overflow: 'hidden', marginTop: 5, }}>
        <TableContainer sx={{ maxHeight:'100vh'}} component={Paper}>
          <Table sx={{ minWidth: 1000 }} stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell>name</TableCell>
                <TableCell align="right">surname</TableCell>
                <TableCell align="right">serial number</TableCell>
                <TableCell align="right">email</TableCell>
                <TableCell align="right">phone number</TableCell>
                <TableCell align="right">gender</TableCell>
                <TableCell align="center">actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.surname}</TableCell>
                  <TableCell align="right">{row.serial_number}</TableCell>
                  <TableCell align="right">{row.email}</TableCell>
                  <TableCell align="right">{row.phone}</TableCell>
                  <TableCell align="right">{row.gender}</TableCell>
                  <TableCell style={{ display:"flex"}} align="center">
                    <Button onClick={() => deleteClick(row.serial_number)} variant="contained" color="error" startIcon={<DeleteIcon />}></Button>
                    <Button variant="contained" color="warning" startIcon={<EditIcon />}></Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        </Paper>
      );

}