import React,{useEffect,useState,Component} from 'react';
import Header from '../components/Header'
import { Grid} from '@mui/material';
import CardStudentCreate from '../components/CardStudentCreate';
import SimpleCRUD from '../components/SimpleCRUD';


export default function Home(props)
{


    return (
        <>
            <Header></Header>
            <Grid container spacing={2}>
           
                <Grid item xs={4}>
                    <CardStudentCreate/>
                </Grid>
                <Grid item xs={8}>
                    <SimpleCRUD/>
                </Grid>
            </Grid>
          
        </>
    );


}