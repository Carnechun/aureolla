import React from "react";
import Universities from "./Universities";
import Home from "./Home";
import {
  BrowserRouter as Router,
  Routes as Switch,
  Route,
  Link,
  useHistory
} from "react-router-dom";
import Careers from "./Careers";
const Routes = () => {
  return (
    <Router>
        <Switch>
          <Route path='/' element={<Home/>} />
          <Route path="careers" element={<Careers />}/>
          <Route path="universities" element={<Universities />}/>
        </Switch>
    </Router>
  );
}

export default Routes;