export default {
    INDEX: 0,
    SHOW: 1,
    DESTROY: 2,
    UPDATE: 3,
    CREATE:4
  }